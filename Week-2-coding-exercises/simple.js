//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

//_________________________________________________________________________________________________________________________________________________________

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    const values = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55,-61,65,-14,-19,-51,-17,-25];

    var positiveOdd = [];
    var negativeEven = [];

    for (var i = 0; i <= values.length; i++) {
        if((values[i] > 0) && (values[i] % 2 != 0)){
            positiveOdd.push(values[i]);
        }
        else if((values[i] < 0) && (values[i] % 2 == 0)){
            negativeEven.push(values[i]);
        }
        else{
            continue;
        }
    }
        
        let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
        outPutArea.innerText = output +"Positive odd: "+ positiveOdd +"\n" + "Negative even: "+ negativeEven//this line will fill the above element with your output.
}


//______________________________________________________________________________________________________________________________________________________
function question2(){
    let output = "" 
    
    //Question 2 here 
    //Part a
    //Declaring variables
    var count = 0;
    var freqOne=0, freqTwo=0, freqThree=0, freqFour=0, freqFive=0, freqSix=0;

    // Iterate 60000 times
    while(count < 60000){
        // Generate random value
        var rand = Math.floor((Math.random() * 6) + 1);

        //Checking the random value with respective number
        if(rand == 1){
            freqOne += rand; //freqOne = freqOne+rand;
        } else if(rand == 2){
            freqTwo += rand;
        } else if(rand == 3){
            freqThree += rand;
        } else if(rand == 4){
            freqFour += rand;
        } else if(rand == 5){
            freqFive += rand;
        } else if(rand == 6){
            freqSix += rand;
        }
        
        //Increment the count
        count ++;
    }


    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output + "Frequency of die rolls \n"+"1: " + freqOne + "\n"+ "2: "+ freqTwo + "\n"+"3: " + freqThree + "\n"+ "4: "+ freqFour+ "\n" +"5: "+ freqFive+ "\n"+ "6: "+ freqSix + "\n" +"\n"
    
}

//_________________________________________________________________________________________________________________________________________________________



function question3(){
    var output = "" 
    
    //Question 3 here
    
    var arr = [0, 0, 0, 0, 0, 0, 0], values = [], i, x, rand;
    
    // Iterate 60000 times
    for (i = 0; i < 60000; i++) {       
        // Generate random value
        rand = Math.floor((Math.random() * 6) + 1); 
        
        // Increment counter and assigned the frequency of the respective number
        arr[rand++]++;                        
    }

    var outPutArea = document.getElementById("outputArea3");
    outPutArea.innerText = output;
    
    //Iterating for the output
    for(x = 1; x < 7; x++){
        values[x] = (x + ": " + arr[x]);
    }

    outPutArea.innerText = "Frequency of die rolls \n" + values[1] + "\n" +values[2] + "\n" +values[3] + "\n" +values[4] + "\n" +values[5] + "\n" +values[6] + "\n";  
}

//_________________________________________________________________________________________________________________________________________________________

function question4(){
    let output = "" 
    
    // Create an object:
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions: ""
    }
    
    // Iterate 60000 times
    for (i = 0; i < 60000; i++) {       
        // Generate random value
        rand = Math.floor((Math.random() * 6) + 1); 
        
        // Increment counter and assigned the frequency of the respective number       
        dieRolls.Frequencies[rand] += 1
    }

    //Calculating the exeptions
    for(prop in dieRolls.Frequencies)
    {
        if (Math.abs(dieRolls.Frequencies[prop]-10000) > 100)
        {
            dieRolls.Exceptions += `${prop} `
        }
    };

    output += "Frequency of dice rolls\n"
    output += `Total rolls: ${dieRolls.Total}\n`
    output +=  "Frequencies:\n"

    for(i = 1; i <= 6; i++)
    {
        output += `${i}: ${dieRolls.Frequencies[i]} \n`
    };

    output += `Exceptions: ${dieRolls.Exceptions}`

    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 

}


//_________________________________________________________________________________________________________________________________________________________

function question5(){
    let output = "";
    var values = [];
    
    // Create an object:
    var person = {name:"Jane", income:127050, tax:0};

    if(person.income <= 18200){
        values[0] = (person.name + "’s income is: $" + person.income + ", and her tax owed is: $" + person.tax);
    }
    else if((18200 < person.income) && (person.income <= 37000)){
        person.tax = ((person.income - 18200) * 19) / 100
        values[1] = (person.name + "’s income is: $" + person.income + ", and her tax owed is: $" + person.tax);
    }
    else if((37000 < person.income) && (person.income <= 90000)){
        person.tax = 3572 + (((person.income - 37000) * 32.5) / 100)
        values[2] = (person.name + "’s income is: $" + person.income + ", and her tax owed is: $" + person.tax);
    }
    else if((90000 < person.income) && (person.income <= 180000)){
        person.tax = 20797 + (((person.income - 90000) * 37) / 100)
        values[3] = (person.name + "’s income is: $" + person.income + ", and her tax owed is: $" + person.tax);
    }
    else if(180000 < person.income){
        person.tax = 54097 + (((person.income - 180000) * 45) / 100)
        values[4] = (person.name + "’s income is: $" + person.income + ", and her tax owed is: $" + person.tax);
    } else {
        person.tax = 0;
    }

    let outPutArea = document.getElementById("outputArea5")  
    outPutArea.innerText = values[3];

}

