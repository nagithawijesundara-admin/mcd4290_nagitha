function doIt() {
    var num1= document.getElementById("number1").value;
    var num2 =  document.getElementById("number2").value;
    var num3 =  document.getElementById("number3").value;
    var result = parseInt(num1) + parseInt(num2) + parseInt(num3);
    
    document.getElementById("answer").innerHTML=result;
    
    if (result >0){
        document.getElementById("answer").className = "positive";
    }else {
        document.getElementById("answer").className = "negative";
     }
    
    
    if (result % 2 == 0){
        document.getElementById("string").innerHTML="(even)";
        document.getElementById("abc").className="even";
    }else {
        document.getElementById("string").innerHTML="(odd)";
        document.getElementById("string").className="odd";
     }
    }