//Q1

var outputAreaRef = document.getElementById("outputArea1");
var output = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}
function sum (num1,num2){
    return num1 + num2;
}

function product (num1,num2){
    return num1 * num2;
}
output += flexible (sum,3,5) + "<br/>";
output += flexible (product,3,5) + "<br/>";
outputAreaRef.innerHTML = output;

//Q2
//part a
/*Using a parameter, pass the object into the function.
In the function, create a variable output.
Using a for loop, iterate through the item.
Using the property and its value, change the variable output.
return the value of the output*/





//part b
var outputAreaRef = document.getElementById("outputArea2");
var output = "";

function objectToHTML(obj){
	let output = '';
	for (let key in obj){
		output += `${String(key)} : ${obj[key]}` + "<br/>"
	}
	return output
}

var testObj = {
	number: 1,
	string: "abc",
	array: [5, 4, 3, 2, 1],
	boolean: true
   };
   

output = objectToHTML(testObj);
outputAreaRef.innerHTML = output;

//Q3
//part a
/*function extremevalues(var val){
    int min, step, x, arr = [4,3,6,12,1,3,8];
	min = arr[0];
	max = arr[0];

	for(x=0; x<=arr.length(); x++){
		step = arr[x];				
		if(step < min){
			min = step; 
		}
		if(step > min){
			max = step; 
		}
	}
	
	print("max =" + max + "Min = " + min);

}

*/



//part b
var outputAreaRef = document.getElementById("outputArea3");
var output = "";

function extremeValues (val) {
	var min, step, x;
	min = val[0];
	max = val[0];

	for(x=0; x<=val.length; x++){
		step = val[x];				
		if(step < min){
			min = step; 
		}
		if(step > max){
			max = step; 
		}
	}
	
	output += max + "<br/>";
    output += min + "<br/>";

}

var values = [4, 3, 6, 12, 1, 3, 8];

extremeValues (values);
outputAreaRef.innerHTML = output;